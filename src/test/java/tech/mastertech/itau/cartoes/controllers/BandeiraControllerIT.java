package tech.mastertech.itau.cartoes.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.cartoes.dto.Bandeira;
import tech.mastertech.itau.cartoes.repositories.BandeiraRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@Sql(scripts = "classpath:dados.sql")
public class BandeiraControllerIT {
  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private BandeiraRepository bandeiraRepository;
  
  private ObjectMapper mapper = new ObjectMapper();
//  private Categoria categoria;
  
//  @Before
//  public void preparar() {
//    categoria = new Categoria();
//    categoria.setNome("Álcool");
//    
//    categoriaRepository.save(categoria);
//  }
  
  @Test
  public void deveBuscarTodasAsBandeiras() throws Exception {
//    List<Categoria> categorias = Lists.list(categoria);
    
    mockMvc.perform(get("/cartaocredito/bandeiras"))
            .andExpect(status().isOk())
            .andExpect(content().string("[{\"id\":1,\"nome\":\"Visa\"}]"));
  }

  @WithMockUser
  @Test
  public void deveSalvarUmaCategoria() throws Exception {
    Bandeira bandeira = new Bandeira();
    bandeira.setNomeBandeira("UmaBandeira");
    String bandeiraJson = mapper.writeValueAsString(bandeira);
    
    mockMvc.perform(
        post("/cartaocredito/bandeira")
        .content(bandeiraJson)
        .contentType(MediaType.APPLICATION_JSON_UTF8)
       )
      .andExpect(status().isOk())
      .andExpect(content().string(containsString(bandeira.getNomeBandeira())));
  }
}
