package tech.mastertech.itau.cartoes.controllers;

import static org.mockito.ArgumentMatchers.any;
//import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.cartoes.dto.Bandeira;
import tech.mastertech.itau.cartoes.services.BandeiraService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = BandeiraController.class)
public class BandeiraControllerTest {
  @Autowired
  private MockMvc mockMvc;
  
  @MockBean
  private BandeiraService bandeiraService;
  
  private ObjectMapper mapper = new ObjectMapper();
  private Bandeira bandeira;
  
  @Before
  public void preparar() {
    bandeira = new Bandeira();
    bandeira.setNomeBandeira("Mastercard");
  }
  
  @Test
  public void deveBuscarTodasAsBandeiras() throws Exception {
    List<Bandeira> bandeiras = Lists.list(bandeira);
    
    when(bandeiraService.getBandeiras()).thenReturn(bandeiras);
    
    mockMvc.perform(get("/cartaocredito/bandeiras"))
            .andExpect(status().isOk())
            .andExpect(content().string(mapper.writeValueAsString(bandeiras)));
  }
  
  @Test
  @WithMockUser
  public void deveSalvarUmaBandeira() throws Exception {
    when(bandeiraService.setBandeira(any(Bandeira.class))).thenReturn(bandeira);
    
    String bandeiraJson = mapper.writeValueAsString(bandeira);
    
    mockMvc.perform(
        post("/cartaocredito/bandeira")
        .content(bandeiraJson)
        .contentType(MediaType.APPLICATION_JSON_UTF8)
       )
      .andExpect(status().isOk())
      .andExpect(content().string(bandeiraJson));
  }
}
