package tech.mastertech.itau.cartoes.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.cartoes.dto.Bandeira;
import tech.mastertech.itau.cartoes.repositories.BandeiraRepository;
import tech.mastertech.itau.cartoes.services.BandeiraService;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = BandeiraService.class)
public class BandeiraServiceTest {
  @Autowired
  private BandeiraService sujeito;
  
  @MockBean
  private BandeiraRepository bandeiraRepository;
  
  @Test
  public void deveSalvarUmaBandeira() {
    //given
    Bandeira bandeira = new Bandeira();
    bandeira.setNomeBandeira("Visa");

    when(bandeiraRepository.save(bandeira)).thenReturn(bandeira);    
    
    //when
    Bandeira bandeiraSalva = sujeito.setBandeira(bandeira);
    
    //expect
    assertEquals(bandeiraSalva.getNomeBandeira(), bandeira.getNomeBandeira());
  }
  
  @Test
  public void deveRetornarListaDeBandeiras() {
    Bandeira bandeira = new Bandeira();
    bandeira.setIdBandeira(1);
    bandeira.setNomeBandeira("Visa");
    
    List<Bandeira> bandeiras = Lists.list(bandeira);
    
    when(bandeiraRepository.findAll()).thenReturn(bandeiras);
    
    Iterable<Bandeira> bandeirasEncontradasIterable = sujeito.getBandeiras();
    List<Bandeira> bandeirasEncontradas = Lists.newArrayList(bandeirasEncontradasIterable);
    
    assertEquals(1, bandeirasEncontradas.size());
    assertEquals(bandeira, bandeirasEncontradas.get(0));
  }
//  
//  @Test(expected = Exception.class)//trabalhar com excessão específica
//  public void deveLancarExcessaoQuandoSalvarCategoriaSemNome() {
//    Categoria categoria = new Categoria();
//    
//    when(categoriaRepository.save(categoria)).thenThrow(new Exception(""));
//    
//    sujeito.setCategoria(categoria);
//  }
}
