package tech.mastertech.itau.cartoes.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cartoes.dto.Bandeira;

public interface BandeiraRepository extends CrudRepository<Bandeira, Integer>{

}
