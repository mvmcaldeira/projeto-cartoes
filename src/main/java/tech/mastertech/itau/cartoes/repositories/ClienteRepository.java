package tech.mastertech.itau.cartoes.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cartoes.dto.Cliente;


public interface ClienteRepository extends CrudRepository<Cliente, String>{
	public Optional<Cliente> findByCpfCliente(String cpfCliente);
}
