package tech.mastertech.itau.cartoes.repositories;


import java.time.LocalDateTime;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cartoes.dto.Lancamento;

public interface LancamentoRepository extends CrudRepository<Lancamento, Long> {
	
//	@Query(nativeQuery=true, value="SELECT * FROM Lancamento WHERE cartao_numero_cartao = :cartao ")
	public Iterable<Lancamento> findAllByCartao_numeroCartaoAndDataLancamentoBetween
	(String cartao, LocalDateTime dataInicio, LocalDateTime dataFim) ;
	
}