package tech.mastertech.itau.cartoes.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Bandeira {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idBandeira;
	
	public int getIdBandeira() {
		return idBandeira;
	}
	public void setIdBandeira(int idBandeira) {
		this.idBandeira = idBandeira;
	}
	private String nomeBandeira;

	public String getNomeBandeira() {
		return nomeBandeira;
	}
	public void setNomeBandeira(String nomeBandeira) {
		this.nomeBandeira = nomeBandeira;
	}
	
	
	
	
	

}
