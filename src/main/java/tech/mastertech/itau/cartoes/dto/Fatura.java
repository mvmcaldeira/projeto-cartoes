package tech.mastertech.itau.cartoes.dto;

import java.util.ArrayList;

public class Fatura {
	

    private double valorTotal;
    
    private Iterable<Lancamento> lancamentos = new ArrayList<>();
    
    public Fatura(Iterable<Lancamento> lancamentos) {
    	
    	for(Lancamento lanc : lancamentos) {
    		valorTotal += lanc.getValorLancamento();
    		
    	}
    	
    	this.lancamentos = lancamentos;
    	
    }
    
	public Iterable<Lancamento> getLancamentos() {
		return lancamentos;
	}
	public double getValorTotal() {
		return valorTotal;
	}

	

}
