package tech.mastertech.itau.cartoes.services;

import java.time.LocalDateTime;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.cartoes.dto.Cartao;
import tech.mastertech.itau.cartoes.dto.Lancamento;
import tech.mastertech.itau.cartoes.repositories.LancamentoRepository;

@Service
public class LancamentoService {

	@Autowired
	private LancamentoRepository lancamentoRepository;

	public Lancamento setLancamento(Lancamento lancamento, Cartao cartao) {

		if (cartao.getisAtivo()) {
			lancamento.setDataLancamento(LocalDateTime.now());
			lancamentoRepository.save(lancamento);
			return lancamento;
		}

		return null;

	}

	// todos os lancamentos da tabela
	public Iterable<Lancamento> getLancamentos() {
		return lancamentoRepository.findAll();
	}

//	public Lancamento getLancamento(long idLancamento) {
//
//		Optional<Lancamento> lancamento = lancamentoRepository.findById(idLancamento);
//
//		if (lancamento.isPresent()) {
//			return lancamento.get();
//		}
//
//		return null;
//
//	}
//
//
//
	public void deleteLancamento(long idLancamento) {

		lancamentoRepository.deleteById(idLancamento);

	}
	
	
	public Iterable<Lancamento> getFatura(String cartao, LocalDateTime dataInicio, LocalDateTime dataFim ) {

		return lancamentoRepository.findAllByCartao_numeroCartaoAndDataLancamentoBetween(cartao, dataInicio, dataFim);
	}

}
