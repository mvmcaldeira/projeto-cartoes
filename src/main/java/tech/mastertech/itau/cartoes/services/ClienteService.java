package tech.mastertech.itau.cartoes.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import tech.mastertech.itau.cartoes.dto.Cliente;
import tech.mastertech.itau.cartoes.repositories.ClienteRepository;


@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	public Iterable<Cliente> getClientes() {
		return clienteRepository.findAll();
	}

	public Cliente setCliente(Cliente cliente) {
		
	    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	    
	    cliente.setSenha(encoder.encode(cliente.getSenha()));
	    
		return clienteRepository.save(cliente);
		
	}

	public void deleteCliente(String cpfCliente) {

		clienteRepository.deleteById(cpfCliente);

	}

	public Cliente getCliente(String cpfCliente) {

		Optional<Cliente> cliente = clienteRepository.findById(cpfCliente);

		if (cliente.isPresent()) {
			return cliente.get();
		}

		return null;

	}
	public Cliente mudarTelefone(String cpfCliente, String telefoneCliente) {

		Cliente cliente = getCliente(cpfCliente);

		if (cliente != null) {
			cliente.setTelefoneCliente(telefoneCliente);
			clienteRepository.save(cliente);
		}

		return cliente;

	}
	
	 public Cliente fazerLogin(Cliente cliente){
		    Optional<Cliente> clienteOptional = 
		        clienteRepository.findByCpfCliente(cliente.getCpfCliente());
		    
		    if(!clienteOptional.isPresent()) {
		      return null;
		    }
		    
		    Cliente clienteSalvo = clienteOptional.get();
		    
		    BCryptPasswordEncoder bPasswordEncoder = new BCryptPasswordEncoder();
		    
		    if(bPasswordEncoder.matches(cliente.getSenha(), clienteSalvo.getSenha())) {
		      return clienteSalvo;
		    }
		    
		    return null;
		  }
}
