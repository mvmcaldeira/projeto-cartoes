package tech.mastertech.itau.cartoes.controllers;

import java.security.Principal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.cartoes.dto.Cartao;
import tech.mastertech.itau.cartoes.services.CartaoService;

@RestController
@RequestMapping("/cartaocredito")
public class CartaoController {
	
	private Cartao retorno;
	
	@Autowired
	private CartaoService cartaoService;
	
	@PostMapping("/auth/cartao")
	public Cartao setCartao(@RequestBody Cartao cartao, Principal principal) {
		
		cartao.getCliente().setCpfCliente(principal.getName());
		
		return cartaoService.setCartao(cartao);
	}
	
	//
	@GetMapping("/auth/cartoes")
	public Iterable<Cartao> getCartoes() {
		return cartaoService.getCartoes();
	}
	
	@GetMapping("/auth/cartao/{numeroCartao}")
	public Cartao getCartao(@PathVariable String numeroCartao) {
		retorno = cartaoService.getCartao(numeroCartao);
		if (retorno == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return retorno;
	}
	
	@DeleteMapping("/auth/cartao/{numeroCartao}")
	public String deleteCartao(@PathVariable String numeroCartao) {
		cartaoService.deleteCartao(numeroCartao);
		
		return "Solicitacao atendida";

	}
	
	//Mudar status de cartao
	@PatchMapping("/auth/cartao/{numeroCartao}")
	public Cartao mudarStatusCartao(@PathVariable String numeroCartao, @RequestBody Map<String, Boolean> isAtivo) {
		
		retorno = cartaoService.mudarStatusCartao(numeroCartao,isAtivo.get("isAtivo"));
		
		if(retorno == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return retorno;
	}
}
