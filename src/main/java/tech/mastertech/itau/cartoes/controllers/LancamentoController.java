package tech.mastertech.itau.cartoes.controllers;

import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mysql.cj.result.LocalDateTimeValueFactory;

import tech.mastertech.itau.cartoes.dto.Cartao;
import tech.mastertech.itau.cartoes.dto.Fatura;
import tech.mastertech.itau.cartoes.dto.Lancamento;
import tech.mastertech.itau.cartoes.services.CartaoService;
import tech.mastertech.itau.cartoes.services.LancamentoService;

@RestController
@RequestMapping("/cartaocredito")
public class LancamentoController {

//	private Lancamento retorno;
	
	@Autowired
	private LancamentoService lancamentoService;
	
	@Autowired
	private CartaoService cartaoService;
	
	private Iterable<Lancamento> lancamentos = new ArrayList<>();
	
	@GetMapping("/lancamentos")
	public Iterable<Lancamento> getLancamento(){
		
		return lancamentoService.getLancamentos();
	}
	
	
	@PostMapping("/lancamento")
	public Lancamento setLancamento(@RequestBody Lancamento lancamento, Principal principal) {
		
		Cartao cartao = new Cartao();
		
		cartao = lancamento.getCartao();
		
		cartao = cartaoService.getCartao(cartao.getNumeroCartao());
		
		return lancamentoService.setLancamento(lancamento,cartao);
	}
	
	
	// estorno do lancamento
	@DeleteMapping("/lancamento/{idLancamento}")
	public String deleteLancamento(@PathVariable long idLancamento) {
		lancamentoService.deleteLancamento(idLancamento);
		
		return "Solicitacao atendida";

	}
	
	@GetMapping("/lancamentos/fatura/{cartao}/{dataInicio}/{dataFim}")
	public Fatura geraFatura(@PathVariable String cartao, 
			@PathVariable String dataInicio,
			@PathVariable String dataFim){
		
		String horaIni;
		horaIni =" 00:00:00.000";
		
		String horaFim;
		horaFim =" 23:59:59.000";
		
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
		
		dataInicio = dataInicio + horaIni;
		dataFim = dataFim + horaFim;
		
	    LocalDateTime dateIni = LocalDateTime.parse(dataInicio, formatter);
	    LocalDateTime dateFim = LocalDateTime.parse(dataFim, formatter);
		
		lancamentos = lancamentoService.getFatura(cartao,dateIni, dateFim);
		
		Fatura fatura = new Fatura(lancamentos);
		
		return fatura;
		
	}
	
}
