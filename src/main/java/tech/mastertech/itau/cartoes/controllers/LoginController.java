package tech.mastertech.itau.cartoes.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.cartoes.dto.Cliente;
import tech.mastertech.itau.cartoes.security.JwtTokenProvider;
import tech.mastertech.itau.cartoes.services.ClienteService;

@RestController
@RequestMapping("/cartaocredito")
public class LoginController {
  
	  @Autowired
	  private ClienteService clienteService;

	  @PostMapping("/login")
	  public Map<String, Object> login(@RequestBody Cliente cliente) {
	    Cliente clienteSalvo = clienteService.fazerLogin(cliente);
	    
	    if(clienteSalvo == null) {
	      throw new ResponseStatusException(HttpStatus.FORBIDDEN);
	    }
	    
	    Map<String, Object> retorno = new HashMap<>();
	    retorno.put("cliente", clienteSalvo);
	    
	    JwtTokenProvider provider = new JwtTokenProvider();
	    String token = provider.criarToken(cliente.getCpfCliente());
	    retorno.put("token", token);
	    
	    return retorno;
	  }
	}


